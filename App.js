import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  Button,
  FlatList,
  Alert,
  ScrollView,
} from 'react-native';


import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

import Message from './app/components/message/Message';

import Body from './app/components/body/Body';

import OurFlatList from './app/components/ourFlatList/OurFlatList';


import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Video from 'react-native-video';

const Settings = [
  {
    id: '1',
    title: 'Account',
    icon: <MaterialCommunityIcons name="account"  size={24}/>
  },
  {
    id: '2',
    title: 'Notificacion',
    icon: <MaterialCommunityIcons name="bell"  size={24}/>
  },
  {
    id: '3',
    title: 'Appereance',
    icon: <MaterialCommunityIcons name="eye"  size={24}/>
  },
  {
    id:'4',
    title:'Privacy & Security',
    icon: <MaterialCommunityIcons name="security"  size={24}/>
  },
  {
    id:'5',
    title:'Help and Support',
    icon: <MaterialCommunityIcons name="help"  size={24}/>
  },
  {
    id:'6',
    title:'About',
    icon: <MaterialCommunityIcons name="information"  size={24}/>
  },
];

function Item({ title, icon }) {
  return (
    <View style={styles.item}>
      <Text >{icon}</Text>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}


const Stack = createStackNavigator();
const StackInterno = createStackNavigator();
//const Tab = createBottomTabNavigator();
const Tab = createMaterialBottomTabNavigator();
//const Tab = createMaterialTopTabNavigator();

function Lista({navigation}) {
  return (
    <Stack.Navigator >
        <Stack.Screen options={{headerShown:false}} name="ListaInterna" component={ListaInterna}/> 
        <Stack.Screen options={{headerShown: false}} name="Details" component={DetailsScreen}/>
    </Stack.Navigator> 
  );
}

function ListaInterna ({route, navigation}){
  return (<OurFlatList navigation={navigation}/>);
}

function DetailsScreen({route, navigation}) {

  const {itemTitle} = route.params;
  //const {itemID} = route.params;
  const {itemDesc} = route.params;
  const {itemPic} = route.params;

  return (
    <View style={{backgroundColor: '#212121', flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      
        <Text style={{color: 'white' , fontSize: 40, marginBottom: 20}}>{itemTitle}</Text>
        <Image style={{aspectRatio: 2/3 , width:250 , marginBottom: 20} } source={{uri: itemPic}} />
        <Text style={{color: 'white' , fontSize: 24, marginBottom: 20}}>Descripcion: {itemDesc}</Text>

      <TouchableOpacity onPress = {() => navigation.navigate('ListaInterna')}>
        <View style = {{padding:10, backgroundColor: '#2196f3', alignItems: 'center', justifyContent: 'center', borderRadius: 4}}>
          <Text style = {{fontSize: 20, color: 'white'}}>VOLVER</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

function SettingScreen ({navigation}) {
  return(
    <View style={{flex:1,  padding:50}}>
      
      <Text style={styles.title}>Setting Screen</Text>
      
      <SafeAreaView style={styles.container}>
        <FlatList
          data={Settings}
          renderItem={({ item }) => <Item icon={item.icon} title={item.title}/>}
          keyExtractor={item => item.id}
        />
      </SafeAreaView>


      <Button
        title="Volver a Home"
        onPress={() =>navigation.navigate('List')}
        />
      
    </View>
  );
}


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      textValue: 0,
      count: 0,
      items: [],
      error: null,
    };
  }

  Tabs = ({Navigation}) => {
    return (
      <Tab.Navigator
          initialRouteName="List" 
          tabBarOptions={{
            activeTintColor: '#e91e63',
        }}>

          <Tab.Screen 
            name="List" 
            component={Lista} 
            options={{
            tabBarLabel: 'List',
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="home" color={color} size={24} />),
          }}
          />

          <Tab.Screen 
            name="Video" 
            component={this.VideoScreen} 
            options={{
            tabBarLabel: 'Video',
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="video" color={color} size={24} />),
          }}
          />

          <Tab.Screen 
            name="Settings" 
            component={SettingScreen} 
            options={{
            tabBarLabel: 'Settings',
            tabBarIcon: ({color, size}) => (<MaterialCommunityIcons name="settings" color={color} size={24} />),
          }}
          />
        </Tab.Navigator>
    );
    
  }


  Login = ({navigation}) => {
    return (
      <View style={{flex: 1, backgroundColor: '#212121', alignItems: 'center', justifyContent: 'center'}}>
        <Text style={{fontSize: 30,color: 'white'}}>Iniciar Sesion</Text>
        <Image style={{height: 150, width:150, alignSelf: 'center'}} source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTVXn0HEgD2bRe8nMGN8owyVCyt_XVN_jogEHYLNG5-R-0_zyvp&usqp=CAU'}} />
  
        <View >
            <Text style={{fontSize: 25, color: 'white'}}> Usuario </Text>
          </View>
  
        <TextInput
            style={{color:'white', height: 40, width: 150, borderColor: 'gray', borderWidth: 1}}
            onChangeText={ (text) => this.setState({username:text})}
            
            placeholder="Usuario"
            placeholderTextColor="gray" 
            />
  
        <View >
            <Text style={{fontSize: 25, color: 'white'}}> Contraseña </Text>
          </View>  
  
        <TextInput
            style={{color:'white', height: 40, width: 150, borderColor: 'gray', borderWidth: 1}}
            secureTextEntry={true}
            onChangeText={ (text) => this.setState({password:text})}
            
            placeholder="Contraseña"
            placeholderTextColor="gray" 
            />
  
        <Button
          title="Ingresar"
          onPress={() => {
            if ( (this.state.username == '')&&(this.state.password == '') ){
              navigation.navigate('TabContainer')
            }else{
              Alert.alert(
                'Datos incorrectos',
                'Verifique los datos ingresados, username:' + this.state.username,
                [
                  {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
            }
            
          }}
        />
      </View>
    );
  }

  VideoScreen = ({navigation}) => {
    return(
      <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
        <Text>Video Screen</Text>

        <Video
          source={{ uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }}
          style={styles.backgroundVideo}
          muted={false}
          controls={true}
          repeat={true}
          resizeMode={"cover"}
          volume={1.0}
          rate={1.0}
          ref={r => this.player = r}
        />
          
        <Button
          title="Volver a Home"
          onPress={() =>navigation.navigate('List')}
          />
      </View>
    );
  }


  componentDidMount() {
    //console.warn('Estamos dentro componentDidMount', this.state.textValue);
  }

  shouldComponentUpdate() {
    //console.warn('Estamos dentro shouldComponentUpdate', this.state.error);
    return true;
  }

  changeTextInput = text => {
    this.setState({textValue: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  showAlert = () => {
    Alert.alert(
      'Datos incorrectos',
      'Verifique los datos ingresados',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  };

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
              <Stack.Screen options={{headerShown:false}} name="Log" component={this.Login}/> 
              <Stack.Screen options={{headerShown:false}} name="TabContainer" component={this.Tabs}/>
            </Stack.Navigator>
      </NavigationContainer>
    );
/*
    <Stack.Navigator>
              <Stack.Screen name="Log" component={this.Login}/> 
              <Stack.Screen name="List" component={Lista}/>
              <Stack.Screen name="Details" component={DetailsScreen} />
    
            </Stack.Navigator>
*/
  }
}

const styles = StyleSheet.create({
  text: {
    alignItems: 'center',
    fontSize: 50,
    padding: 10,
  },
  button: {
    top: 10,
    fontSize: 50,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
  container: {
    flex: 1,
    marginTop: 2,
  },
  icon_style:{
    fontSize:50
  },
  item: {
    flexWrap: 'wrap', 
    alignItems: 'center',
    flexDirection:'row',
    backgroundColor: '#e2e2e2',
    padding: 10,
    marginVertical: 5,
    marginHorizontal: 1,
  },
  title: {
    fontSize: 24,
    marginStart:10
  },
  backgroundVideo: {
    position:'absolute',
    height:700,
    top: 0,
    left: 0,
    bottom:0,
    right:0
    },
});
